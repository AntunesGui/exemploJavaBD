import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlunoDao {
	private Connection conexao;

	public AlunoDao() {
		this.conexao = Conexao.getConnection();
	}

	public void inserir(Aluno aluno) {
		String query = "insert into aluno(serie,nome_turma, nome_aluno, nome_logradouro, numero_logradouro,cep)"
				+ " values(?,?,?,?,?,?)";
		try {

			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setInt(1, aluno.getSerie());
			statement.setString(2, aluno.getNomeTurma());
			statement.setString(3, aluno.getNomeAluno());
			statement.setString(4, aluno.getNomeLogradouro());
			statement.setInt(5, aluno.getNumeroLogradouro());
			statement.setInt(6, aluno.getCep());

			statement.execute();
			statement.close();						

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

//	public List<Aluno> consultar(){
//
//		String query = "select * from aluno";
//		List<Aluno> alunos = new ArrayList<Aluno>();		
//
//		try {
//			PreparedStatement statement = conexao.prepareStatement(query);
//
//			ResultSet resultado = statement.executeQuery();
//
//			while(resultado.next()) {
//
//				Aluno aluno = new Aluno();
//				aluno.setSerie(resultado.getInt("serie"));
//				aluno.setNomeTurma(resultado.getString("nome_turma"));
//				aluno.setNomeAluno(resultado.getString("nome_aluno"));
//				aluno.setNomeLogradouro(resultado.getString("nome_logradouro"));
//				aluno.setNumeroLogradouro(resultado.getInt("numero_logradouro"));
//				aluno.setCep(resultado.getInt("cep"));
//
//				alunos.add(aluno);
//			}
//
//			resultado.close();
//			statement.close();
//			return alunos;
//
//		}catch (SQLException e) {
//			throw new RuntimeException(e);
//		}
//	}

	public void deletar(int numeroMatricula) {
		String query = "delete from aluno where numero_matricula = ?";
		try {

			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setInt(1, numeroMatricula);

			statement.execute();
			statement.close();						

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Aluno> consultar(String nome){

		String query = "select * from aluno where nome_aluno = ?";
		List<Aluno> alunos = new ArrayList<Aluno>();		

		try {
			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setString(1, nome);
			
			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				
				Aluno aluno = new Aluno();
				aluno.setSerie(resultado.getInt("serie"));
				aluno.setNomeTurma(resultado.getString("nome_turma"));
				aluno.setNomeAluno(resultado.getString("nome_aluno"));
				aluno.setNomeLogradouro(resultado.getString("nome_logradouro"));
				aluno.setNumeroLogradouro(resultado.getInt("numero_logradouro"));
				aluno.setCep(resultado.getInt("cep"));
				
				alunos.add(aluno);
			}

			resultado.close();
			statement.close();
			return alunos;

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
//	public List<Aluno> consultar(String nome){
//
//		String query = "select * from aluno where nome_aluno = ?";
//		List<Aluno> alunos = new ArrayList<Aluno>();		
//
//		try {
//			PreparedStatement statement = conexao.prepareStatement(query);
//
//			statement.setString(1, nome);
//			
//			ResultSet resultado = statement.executeQuery();
//
//			while(resultado.next()) {
//				
//				Aluno aluno = new Aluno();
//				aluno.setSerie(resultado.getInt("serie"));
//				aluno.setNomeTurma(resultado.getString("nome_turma"));
//				aluno.setNomeAluno(resultado.getString("nome_aluno"));
//				aluno.setNomeLogradouro(resultado.getString("nome_logradouro"));
//				aluno.setNumeroLogradouro(resultado.getInt("numero_logradouro"));
//				aluno.setCep(resultado.getInt("cep"));
//				
//				alunos.add(aluno);
//			}
//
//			resultado.close();
//			statement.close();
//			return alunos;
//
//		}catch (SQLException e) {
//			throw new RuntimeException(e);
//		}
//	}
	
	public void update(String nomeAntigo, String nomeNovo) {
		String query = "update aluno set nome_aluno = ? where nome_aluno = ?";
		try {

			PreparedStatement statement = conexao.prepareStatement(query);

			statement.setString(1, nomeNovo);
			statement.setString(2, nomeAntigo);


			statement.execute();
			statement.close();						

		}catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}