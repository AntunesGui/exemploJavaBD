import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;


public class Conexao {
	public static Connection getConnection() {
		try {
			return DriverManager.getConnection(
					"jdbc:mysql://localhost/ESCOLA",
					"mastertech",
					"Mastertech@123");
		}	
		catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
